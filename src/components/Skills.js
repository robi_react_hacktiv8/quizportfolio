import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'

export default class Skills extends Component{
    render(){
        return(
            <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="skills">
            <div class="w-100">
              <h2 class="mb-5">Skills</h2>
      
              <div class="subheading mb-3">Programming Languages &amp; Tools</div>
              <ul class="fa-ul mb-0">
                <li>
                  <FontAwesomeIcon icon={faCheckCircle} style={{color:"green"}}/> PHP</li>
                <li>
                  <FontAwesomeIcon icon={faCheckCircle} style={{color:"green"}}/> Android</li>                
              </ul>
            </div>
          </section>     
        )
    }
}