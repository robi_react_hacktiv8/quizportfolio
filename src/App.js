import React, {Component} from 'react';
import './assets/css/bootstrap.css';
import './assets/css/resume.css';
import {BrowserRouter} from 'react-router-dom';
import SideNavigation from './components/SideNavigation';
import About from './components/About';
import Experince from './components/Experience';
import Education from './components/Education';
import Skills from './components/Skills';
import Interest from './components/Interest';
import Awards from './components/Awards';

export default class App extends Component {
  render(){
    return (
      <BrowserRouter>
      <div>  
        <SideNavigation/> 
        <hr className="m-0" /> 
        <About/>
        <hr className="m-0" />
        <Experince/>
        <hr className="m-0" />
        <Education/>
        <hr className="m-0" />
        <Skills/>
        <hr className="m-0" />
        <Interest/>
        <hr className="m-0" />
        <Awards/>
      </div>
      </BrowserRouter>
    )
  }
}
